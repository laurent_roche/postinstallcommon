#!
# include file by the my.... command series (post installation scripts)
#

DOWNLOAD_DIR="/tmp/"
MY_PROG=$(basename $0 .sh)
GUI=""
ALL_PPA_INSTALL=""
ALL_PPA_NS=""
MY_STDERR="/tmp/$$_mc_postinst.stderr"

# Set language with given parameter
if [ "X$1" != "X" ]
then
	LANG=$1
fi

# Load the Language file according to default language 
f_get_msg(){
	no_msg_file="Error: message file not found or has wrong permissions."
	lang=${LANG:=english}
	case ${lang} in
		[Ee][Nn][Gg]*	)	all_msg=${MY_DIR}/${MY_PROG}.eng 
							DOWNLOAD_DIR=~/Downloads/;;
		[Ff][Rr]*	)	all_msg=${MY_DIR}/${MY_PROG}.fr 
						DOWNLOAD_DIR=~/Téléchargements/;;
		*			)	all_msg=${MY_DIR}/${MY_PROG}.eng
						DOWNLOAD_DIR=~/Downloads/;;
	esac
	if [ -f ${all_msg} -a -r ${all_msg} ]
	then
		. ${all_msg}
	else
		echo ${no_msg_file}
		exit 3
	fi
}

# Run Action $1 with command $2 and display notification $3, if given
f_action_exec() {
	if [[ "$GUI" = *"$1"* ]]
	then
		if [ "X$3" = "X" ]
		then
			ns_exec="$1"
		else
			ns_exec="$3"
		fi
		echo "$NS_MARK_TITLE $NS_INSTALL $ns_exec ..."
		echo ""
		notify-send -i system-software-update "$MY_PROG $NS_INSTALL $ns_exec" -t 5000
		dash -c "$2" 2> >(tee -a $MY_STDERR >&2)
		if [ $? -ne 0 ] ; then 
			notify-send -i dialog-error "$MY_PROG" "$NS_ERR_INSTALL $ns_exec" -t 5000
			echo "$NS_ERR_INSTALL $ns_exec" >> $MY_STDERR
		fi
	fi
}

# Run Action $1 by installing $2 and display notification
f_action_install() {
	if [[ "$GUI" == *"$1"* ]]
	then
		echo "$NS_MARK_TITLE $NS_INSTALL $1 ..."
		echo ""
		notify-send -i system-software-update "$MY_PROG" "$NS_INSTALL $1" -t 5000
		#apt-get required to be able to use wildcard * in package name
		sudo apt-get -y install $2 2> >(tee -a $MY_STDERR >&2)
		if [ $? -ne 0 ] ; then 
			notify-send -i dialog-error "$MY_PROG" "$NS_ERR_INSTALL $1" -t 5000
			echo "$NS_ERR_INSTALL $1" >> $MY_STDERR
		fi
	fi
}

# Run Action $1 by adding source $2 to $3 and installing $4 and display notification
f_action_add-source_install() {
	if [[ "$GUI" == *"$1"* ]]
	then
		echo "$NS_MARK_TITLE $NS_INSTALL $1 ..."
		echo ""
		notify-send -i system-software-update "$MY_PROG" "$NS_INSTALL $1" -t 5000
        sudo sh -c "echo \"$2\" >> /etc/apt/sources.list.d/${3}" 
		sudo apt update
		sudo apt install $4
		if [ $? -ne 0 ] ; then 
			notify-send -i dialog-error "$MY_PROG" "$NS_ERR_INSTALL $1" -t 5000
			echo "$NS_ERR_INSTALL $1" >> $MY_STDERR
		fi
	fi
}


# Run Action $1 by adding key $3, create repo file with line $2 and installing $4 and display notification
f_action_create_ppa_key_install() {
	if [[ "$GUI" == *"$1"* ]]
	then
		echo  "$NS_MARK_TITLE $NS_DEFERED $1 ..."
		echo ""
		notify-send -i system-software-update "$MY_PROG" "$NS_DEFERED $1" -t 5000
		ALL_PPA_NS="${ALL_PPA_NS} $1"
		wget -qO- "$3"  | sudo tee /etc/apt/trusted.gpg.d/${4}.asc
		echo "$2" | sudo tee /etc/apt/sources.list.d/${4}-stable.list > /dev/null
		if [ $? -ne 0 ] ; then 
			notify-send -i dialog-error "$MY_PROG" "$NS_ERR_CREATE_REPO $1" -t 5000
			echo "$NS_ERR_CREATE_REPO $1" >> $MY_STDERR
		fi
		ALL_PPA_INSTALL="${ALL_PPA_INSTALL} $4"
	fi
}

# Run Action $1 by adding key $3 plus ppa $2 and installing $4 and display notification
f_action_ppa_key_install() {
	if [[ "$GUI" == *"$1"* ]]
	then
		echo  "$NS_MARK_TITLE $NS_DEFERED $1 ..."
		echo ""
		notify-send -i system-software-update "$MY_PROG" "$NS_DEFERED $1" -t 5000
		ALL_PPA_NS="${ALL_PPA_NS} $1"
		wget -qO- "$3"  | sudo tee /etc/apt/trusted.gpg.d/${4}.asc
		sudo add-apt-repository -yn $2 2> >(tee -a $MY_STDERR >&2)
		if [ $? -ne 0 ] ; then 
			notify-send -i dialog-error "$MY_PROG" "$NS_ERR_ADD_REPO $1" -t 5000
			echo "$NS_ERR_ADD_REPO $1" >> $MY_STDERR
		fi
		ALL_PPA_INSTALL="${ALL_PPA_INSTALL} $4"
	fi
}

# Run Action $1 by adding ppa $2 and installing $3 and display notification
f_action_ppa_install() {
	if [[ "$GUI" == *"$1"* ]]
	then
		echo  "$NS_MARK_TITLE $NS_DEFERED $1 ..."
		echo ""
		notify-send -i system-software-update "$MY_PROG" "$NS_DEFERED $1" -t 5000
		ALL_PPA_NS="${ALL_PPA_NS} $1"
		sudo add-apt-repository -yn $2 2> >(tee -a $MY_STDERR >&2)
		if [ $? -ne 0 ] ; then 
			notify-send -i dialog-error "$MY_PROG" "$NS_ERR_ADD_REPO $1" -t 5000
			echo "$NS_ERR_ADD_REPO $1" >> $MY_STDERR
		fi
		ALL_PPA_INSTALL="${ALL_PPA_INSTALL} $3"
	fi
}

# Run Action $1 by downloading $2 and installing the downloaded file (end of $2), and display notification
f_action_get() {
	if [[ "$GUI" == *"$1"* ]]
	then
		echo "$NS_MARK_TITLE $NS_INSTALL $1 ..."
		echo ""
		notify-send -i system-software-update "$MY_PROG" "$NS_INSTALL $1" -t 5000
		theSoftToInstall="$1"
		shift
		cd $DOWNLOAD_DIR
		if [[ "X$1" == "Xallow-downgrades" ]]
		then
			theOptions="--allow-downgrades"
			shift
		else
			theOptions=""
		fi
		while (( "$#" ))
		do
			[ -f ./"${1##*/}" ] && /bin/rm ./"${1##*/}"
			wget "$1" 2> >(tee -a $MY_STDERR >&2)
			sudo apt $theOptions -y install ./"${1##*/}" 2> >(tee -a $MY_STDERR >&2)
			if [ $? -ne 0 ] ; then 
				notify-send -i dialog-error "$MY_PROG" "$NS_ERR_INSTALL $theSoftToInstall" -t 5000
				echo "$NS_ERR_INSTALL $theSoftToInstall" >> $MY_STDERR
			fi
			shift
		done
		cd -
	fi
}

# Run Action $1 by downloading $2 and authorising the downloaded file (end of $2) to execute, and display notification
f_action_get_appimg() {
	if [[ "$GUI" == *"$1"* ]]
	then
		echo "$NS_MARK_TITLE $NS_INSTALL $1 ..."
		echo ""
		notify-send -i system-software-update "$MY_PROG" "$NS_INSTALL $1" -t 5000
		cd $DOWNLOAD_DIR
		wget "$2" 2> >(tee -a $MY_STDERR >&2)
		if [ $? -ne 0 ] ; then 
			notify-send -i dialog-error "$MY_PROG" "$NS_ERR_INSTALL $1" -t 5000
			echo "$NS_ERR_INSTALL $1" >> $MY_STDERR
		fi
		chmod u+x "${2##*/}"
		cd -
	fi
}

#send back TRUE/FALSE according the choice defined in $CHK_REP, and the default value given in $1
chkDef() {
	case "$CHK_REP" in
		"$BGN_DEF") echo -n "$1" ;;
		"$BGN_CHECKED") echo -n "TRUE";;
		"$BGN_UNCHECKED") echo -n "FALSE";;
	esac
}

# Clear the Terminal
#clear

#Install tools for the script
which zenity > /dev/null
if [ $? = 1 ]
then
	sudo apt -y install zenity
fi

which notify-send > /dev/null
if [ $? = 1 ]
then
	sudo apt -y install libnotify-bin
fi

which add-apt-repository > /dev/null
if [ $? = 1 ]
then
	sudo apt install -y software-properties-common
fi


#Get Language message
f_get_msg

notify-send  --icon=dialog-question "$NS_WATCH_OUT" "$NS_PWD_ASKED" -t 10000

